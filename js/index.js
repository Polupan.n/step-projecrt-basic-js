"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];

const template = document.querySelector(`#trainer-card`);
const trainersCards = document.querySelector(`.trainers-cards`);
const trainersCardsContainer = document.querySelector(
  `.trainers-cards__container`
);

let fragment = document.createDocumentFragment();

function createTrainerCard(value) {
  fragment = template.content.cloneNode(true);
  const listItem = fragment.querySelector(`li`);
  const name = `${value["last name"]} ${value["first name"]}`;
  let photo = fragment.querySelector(`.trainer__img`);
  let trainerName = fragment.querySelector(`.trainer__name`);
  trainerName.textContent = name;
  listItem.setAttribute("data-experience", `${value.experience}`);
  photo.src = value.photo;
  trainersCardsContainer.append(fragment);
}

DATA.forEach((value) => {
  createTrainerCard(value);
});

document.querySelector(`.sidebar`).removeAttribute(`hidden`);
document.querySelector(`.sorting`).removeAttribute(`hidden`);

trainersCards.addEventListener("click", (event) => {
  const trainerTarget = event.target.closest(".trainer__show-more");
  if (!trainerTarget) return;

  const trainerCard = trainerTarget.closest(".trainer");
  const trainerPhoto = trainerCard
    .querySelector(".trainer__img")
    .getAttribute("src");

  const templateModal = document.querySelector("#modal-template");
  const cloneTemplateModal = templateModal.content.cloneNode(true);
  const modal = cloneTemplateModal.querySelector(".modal");
  let modalImg = modal.querySelector(".modal__img");
  let modalName = modal.querySelector(".modal__name");
  let modalPointCategory = modal.querySelector(".modal__point--category");
  let modalPointExperience = modal.querySelector(".modal__point--experience");
  let modalPointSpecialization = modal.querySelector(
    ".modal__point--specialization"
  );
  let modalText = modal.querySelector(".modal__text");
  let modalClose = modal.querySelector(".modal__close");

  document.body.style = "overflow: hidden";

  DATA.forEach((element) => {
    if (trainerPhoto === element.photo) {
      modalImg.src = element.photo;
      modalName.textContent = `${element["last name"]} ${element["first name"]}`;
      modalPointCategory.textContent = `Категорія: ` + element.category;
      modalPointExperience.textContent = `Досвід: ` + element.experience;
      modalPointSpecialization.textContent =
        `Напрям тренера: ` + element.specialization;
      modalText.textContent = element.description;
    }
  });

  trainersCardsContainer.before(modal);

  modalClose.addEventListener("click", () => {
    document.body.style = "overflow: auto";
    modal.remove();
  });
});

const sortingButtons = document.querySelectorAll(".sorting__btn");

sortingButtons.forEach((button) => {
  button.addEventListener("click", () => {
    sortingButtons.forEach((btn) =>
      btn.classList.remove("sorting__btn--active")
    );
    button.classList.add("sorting__btn--active");
    switch (button.textContent.trim()) {
      case "ЗА ПРІЗВИЩЕМ":
        sortCardsByLastName();
        break;
      case "ЗА ДОСВІДОМ":
        sortCardByExperience();
        break;
      default:
        sortCardsDefault();
    }
  });
});
const trainers = [...document.querySelectorAll(".trainer")];
const cloneTrainers = trainers.map((value) => value);
function sortCardsByLastName() {
  console.log(trainers);
  trainers.sort((a, b) => {
    const nameA = a.querySelector(".trainer__name").textContent.split(` `)[1];
    const nameB = b.querySelector(".trainer__name").textContent.split(` `)[1];
    const lastNameA = a
      .querySelector(".trainer__name")
      .textContent.split(` `)[0];
    const lastNameB = b
      .querySelector(".trainer__name")
      .textContent.split(` `)[0];
    if (lastNameA === lastNameB) {
      return nameA.localeCompare(nameB);
    }
    return lastNameA.localeCompare(lastNameB);
  });
  fragment.append(...trainers);
  trainersCardsContainer.append(fragment);
}

function sortCardsDefault() {
  fragment.append(...cloneTrainers);
  trainersCardsContainer.append(fragment);
}

function sortCardByExperience() {
  trainers.sort((a, b) => {
    return (
      +b.dataset.experience.slice(0, 2) - +a.dataset.experience.slice(0, 2)
    );
  });
  fragment.append(...trainers);
  trainersCardsContainer.append(fragment);
}

const filtersSubmit = document.querySelector(`.filters__submit`);
const buttonsRadio = [...document.querySelectorAll(`[type = "radio"]`)];

filtersSubmit.addEventListener(`click`, (element) => {
  element.preventDefault();
  const checkArr = [];
  buttonsRadio.filter((el) => {
    if (el.checked) {
      checkArr.push(el.labels[0].textContent.trim().toLowerCase());
    }
  });
  const specialization = checkArr[0].toLowerCase();
  const category = checkArr[1].toLowerCase();
  let filteredArr = DATA.filter((el) => {
    if (
      (category === "всі" || category === el.category) &&
      (specialization === "всі" ||
        specialization === el.specialization.toLowerCase())
    ) {
      return true;
    }
  });

  trainers.forEach((value) => {
    value.style.display = `none`;
  });

  filteredArr.forEach((el) => {
    trainers.find((element) => {
      if (
        el.photo === element.querySelector(".trainer__img").getAttribute("src")
      ) {
        element.style.display = `flex`;
      }
    });
  });
});
